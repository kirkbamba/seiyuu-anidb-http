import sys

seiyuu_db = []
seiyuu_list = []
seiyuu_idx  = {}

class Seiyuu:
    def __init__(self, name, id, anime = set(), roles = list()):
        self.name = name
        self.id = id
        self.anime = anime
        self.roles = roles[:]
    def add_role(self, anime_id, character_name):
        self.anime.add(anime_id)
        self.roles.append((anime_id, character_name))
    def write(self):
        print(self.name)
        print(self.id)
        print(len(self.anime))
        print(*self.anime)
        print(len(self.roles))
        for anime_id, character_name in self.roles:
            print('{} {}'.format(anime_id, character_name))
        print()

def read_seiyuu(blob):
    blob = blob.split('\n')
    name = blob[0]
    id = blob[1]
    anime = set(blob[3].split())
    roles = []
    for role in blob[5:]:
        anime_id = role.split()[0]
        character_name = role[len(anime_id) + 1:]
        roles.append((anime_id, character_name))
    return Seiyuu(name, id, anime, roles)

def add_seiyuu(name, id):
    seiyuu = Seiyuu(name, id)
    seiyuu_db.append(seiyuu)
    seiyuu_list.append(id)
    seiyuu_idx[id] = len(seiyuu_list) - 1

def get_seiyuu_list():
    sys.stdin = open('db/seiyuu_list', 'r')
    n = int(input() + input())
    for i in range(n):
        id = input()
        seiyuu_list.append(id)
        seiyuu_idx[id] = i
    sys.stdin.close()

def get_seiyuu_db():
    sys.stdin = open('db/seiyuu', 'r')
    arr = sys.stdin.read().split('\n\n')
    sys.stdin.close()
    
    n = int(arr[0])
    for i in range(n):
        seiyuu_db.append(read_seiyuu(arr[i+1]))

def update_seiyuu_list():
    sys.stdout = open('db/seiyuu_list', 'w')
    print(len(seiyuu_list))
    print()
    for id in seiyuu_list:
        print(id)
    sys.stdout.close()

def update_seiyuu_db():
    sys.stdout = open('db/seiyuu', 'w')
    print(len(seiyuu_db))
    print()
    for seiyuu in seiyuu_db:
        seiyuu.write()
    sys.stdout.close()

def update():
    update_seiyuu_db()
    update_seiyuu_list()

def add_roles(anime_id, characters):
    get_seiyuu_list()
    get_seiyuu_db()
    
    for character in characters:
        name = character.seiyuu_name
        id = character.seiyuu_id
        try:
            idx = seiyuu_idx[id]
        except:
            add_seiyuu(name, id)
            idx = seiyuu_idx[id]
        seiyuu_db[idx].add_role(anime_id, character.name)
    
    update()

def main():
    return

if __name__ == '__main__':
    main()
