import sys
import scrape as sc
import anime_list as al
import seiyuu_list as sl

def add_anime(anime_id):
    sc.download(anime_id)
    title = sc.get_title(anime_id)
    characters = sc.get_characters(anime_id)
    
    is_new = al.add_anime(anime_id, title, [(character.seiyuu_id, character.name) for character in characters])
    
    if is_new:
        sl.add_roles(anime_id, characters)

def main():
    anime_id = sys.argv[1]
    add_anime(anime_id)
    return

if __name__ == '__main__':
    main()
