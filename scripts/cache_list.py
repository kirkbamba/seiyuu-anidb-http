import sys

cache_list = []

def clear():
    sys.stdout = open('cache/list', 'w')
    print('0\n\n', end='')
    sys.stdout.close()

def get_cache_list():
    global cache_list
    
    sys.stdin = open('cache/list', 'r')
    n = int(input())
    input()
    for _ in range(n):
        cache_list.append(input())
    sys.stdin.close()

def update_cache_list():
    sys.stdout = open('cache/list', 'w')
    print(len(cache_list))
    print()
    for cached in cache_list:
        print(cached)
    sys.stdout.close()

def add_to_cache(anime_id):
    cache_list.append(anime_id)
    update_cache_list()

def main():
    return

if __name__ == '__main__':
    main()
