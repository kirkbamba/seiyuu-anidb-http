import sys

initialized = 0
client = ''
clientver = ''

def read_config():
    global initialized
    global client
    global clientver
    
    with open('.config', 'r') as config:
        initialized = int(config.readline().strip())
        client = config.readline().strip()
        clientver = config.readline().strip()

def change_config():
    global initialized
    global client
    global clientver
    
    initialized = 1
    client = input('client: ').strip()
    clientver = input('clientver: ').strip()
    
    sys.stdout = open('.config', 'w')
    print('{}\n{}\n{}'.format(initialized, client, clientver))
    sys.stdout.close()

def main():
    change_config()
    return

if __name__ == '__main__':
    main()
