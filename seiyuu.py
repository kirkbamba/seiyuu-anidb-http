import sys
import os

help_message = \
"""List seiyuu and their most recently watched roles, sorted by how many anime the seiyuu appears in
    seiyuu list_seiyuu
List seiyuu in an anime, sorted by how many anime the seiyuu appears in
    seiyuu list_seiyuu <AniDB anime ID>
List roles of a seiyuu, sorted by most recently watched
    seiyuu list_roles <AniDB seiyuu ID>
List X most recently watched anime (default is X=5)
    seiyuu list_anime [X]
Mark anime as most recently watched, and add it to database if it is new
    seiyuu anime <AniDB anime ID>
Clear anime and seiyuu databases
    seiyuu clear
Change client and clientver
    seiyuu config"""

def main():
    def helper():
        ret = __file__[:-len('seiyuu.py')]
        if not ret:
            ret = './'
        return ret
    os.chdir(helper())
    sys.path.append('scripts')
    
    import config as cf
    cf.read_config()
    if not cf.initialized:
        print('Configure client and clientver.')
        cf.change_config()
        
        import clear
        clear.clear()
        import cache_list
        cache_list.clear()
        return
    
    argc = len(sys.argv)
    if argc == 1:
        print(help_message)
        return
    
    command = sys.argv[1]
    if command not in ['list_seiyuu', 'list_roles', 'list_anime', 'anime', 'clear', 'config']:
        print(help_message)
        return
    
    if command == 'list_seiyuu':
        if argc == 2:
            import list_seiyuu_all as cmd
            cmd.list_seiyuu_all()
        else:
            import list_seiyuu_anime as cmd
            cmd.list_seiyuu_anime(sys.argv[2])
    elif command == 'list_roles':
        if argc == 2:
            print(help_message)
            return
        import list_roles as cmd
        cmd.list_roles(sys.argv[2])
    elif command == 'list_anime':
        import list_anime as cmd
        if argc == 2:
            cmd.list_anime()
        else:
            cmd.list_anime(int(sys.argv[2]))
    elif command == 'anime':
        if argc == 2:
            print(help_message)
            return
        import add_anime as cmd
        cmd.add_anime(sys.argv[2])
    elif command == 'clear':
        import clear
        clear.clear()
    else:
        cf.change_config()
    
    return

if __name__ == '__main__':
    main()
